function Slider(container, navigation) {
    this.container = container;
    this.navigation = navigation;

    this.images = this.container.find('img');
    this.imgWidth = this.images[0].width;
    this.imagesLength = this.images.length;

    this.currentImage = 0;
}

Slider.prototype.transition = function(coordinates) {
    this.container.animate({
        'margin-left': coordinates || -(this.currentImage * this.imgWidth)
    });
};

Slider.prototype.setCurrentPosistion = function(direction) {
    var position = this.currentImage;

    var currentDirection = direction.index();
    
    this.currentImage = currentDirection % this.imagesLength;
    
    return position;
};