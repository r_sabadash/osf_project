jQuery(document).ready(function() {
    // Save email
    var userEmail = $('.social__input');
    var saveEmailButton = $('.social__submit');
    var searchInput = $('.search__input');
    var searchResultBlock = $('.search__result');

    saveEmailButton.on('click', function( e ) {
        e.preventDefault();

        var newEmail = userEmail.val();

        $.ajax({
            type: 'POST',
            url: '../action/email.php',
            data: { newEmail: newEmail }
        }).done(function(data) { 
            userEmail.val('');
        });
    });

    // Search
    searchInput.on('keyup', function() {
        var searchValue = $.trim( searchInput.val() );
        var regex = new RegExp(searchValue, 'i');
        var result = [];

        if (searchValue.length > 0) {
            searchResultBlock.empty();

            $.getJSON( '../json/shoes.json', function( data ) {
                $.each(data.shoes, function( item, shoe ) {
                    if ( shoe.search(regex) != -1 ) {
                        result.push( '<li class="search__item">' + shoe + '</li>' );
                    }
                });

                searchResultBlock.append(result);
            });
        } else {
            searchResultBlock.empty();
        }
    });

    // Modal Login
    var modalWindowLogin = $('#js-modal-login');
    var buttonLogin = $("#js-enter-button");
    var buttonCloseModal = $(".modal__close");

    buttonLogin.on('click', function( e ) {
        modalWindowLogin.fadeIn();
        $('body').css('overflow', 'hidden');
    });

    buttonCloseModal.on('click', function( e ) {
        $(this).closest('.modal').fadeOut();
        $('body').css('overflow', 'auto');
    });

    // Modal Flickr
    var modalWindowFlickr = $('#js-modal-flickr');
    var flickrImage = $('.flickr__image');
    var modalImage = $('.modal__image');

    flickrImage.on('click', function() {
        modalWindowFlickr.fadeIn();
        $('body').css('overflow', 'hidden');
        var srcLink = $(this).attr( 'src' );
        modalImage.attr( 'src', srcLink );
    });

    // Change width
    var buttonFullWidthView = $('#js-full-width-view');
    var buttonColumnsView = $('#js-columns-view');

    function changeProductView( element, addedClass, removedClass ) {
        element.parent().parent().addClass(addedClass).removeClass(removedClass);
    }

    buttonFullWidthView.on('click', function() {
        changeProductView( $('.product'), 'col-full', 'col-3' );
    });

    buttonColumnsView.on('click', function() {
        changeProductView( $('.product'), 'col-3', 'col-full' );
    });

    // Twitter
    var twitterBlock = $('#js-twitter');

    function appendTweets( data ) {
        var text, indexLink, link, date, node, linkNode;

        node = $('<div class="twitter__item"></div>');
        
        $.each( data, function( item ) {
            text = data[item].text;

            date = data[item].created_at;
            date = date.slice( 4, 16 );
            
            node.append('<div class="twitter__text">'+text+'</div>');
            node.append('<div class="twitter__date">'+date+'</div>');
            node.append(linkNode);
        });
        
        twitterBlock.append(node);
    }

    function getTweets() {
        twitterBlock.fadeOut();
        $.ajax({
            type: 'GET',
            url: "../action/get-tweets.php",
            cache: false,
            data: { q: 'sport', count: 2 }
        }).done(function( data ) {
            twitterBlock.empty();
            var objectData = JSON.parse( data );

            appendTweets( objectData.statuses );
        });
        twitterBlock.fadeIn();
    };
    getTweets();

    setInterval( getTweets, 5000 );

    // Slider
    var slider = new Slider($('.slider__block'), $('.slider__navigation'));

    slider.navigation.find('.slider__button').on('click', function() {
        slider.setCurrentPosistion($(this));
        slider.transition();
        $(this).addClass('slider__button_active');
        $(this).siblings().removeClass('slider__button_active');
    });
});