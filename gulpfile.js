var gulp 		 = require('gulp'),
	sass 		 = require('gulp-sass'),
	browserSync  = require('browser-sync'),
	uglify		 = require('gulp-uglifyjs'),
	cssnano		 = require('gulp-cssnano'),
	rename		 = require('gulp-rename'),
	del			 = require('del'),
	autoprefixer = require('gulp-autoprefixer');
// sass
gulp.task('sass', function() {
	return 	gulp.src('sass/**/*.sass')
			.pipe(sass({
				includePaths: require('node-normalize-scss').includePaths
			}))
			.pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
			.pipe(gulp.dest('css'))
			.pipe(browserSync.reload({
				stream: true
			}))
});
// minifacation js main file
gulp.task('script-main', function() {
	return 	gulp.src([
				'js/main.js'
			])
			.pipe(uglify())
			.pipe(rename({
				suffix: '.min'
			}))
			.pipe(gulp.dest('js'))
			.pipe(browserSync.reload({
				stream: true
			}))
});
// minifacation css
gulp.task('css-libs', function() {
	return 	gulp.src([
				'css/main.css',
			])
			.pipe(cssnano())
			.pipe(rename({
				suffix: '.min'
			}))
			.pipe(gulp.dest('css'))
			.pipe(browserSync.reload({
				stream: true
			}))
});
// browser-sync (live reload)
gulp.task('browser-sync', function() {
	return 	browserSync({
			server: {
				baseDir: ''
			},
			notify: false
	})
});
// watcher
gulp.task('watch', ['browser-sync', 'sass', 'css-libs', 'script-main'], function() {
	gulp.watch('sass/**/*.sass', ['sass']);
	gulp.watch('*.html', browserSync.reload);
	gulp.watch('css/**/*.css', ['css-libs']);
	gulp.watch('js/**/*.js', ['script-main', browserSync.reload]);
});