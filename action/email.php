<?php
if ( isset($_POST['newEmail']) ) {
    $jsonFile = 'email.json';

    $fh = fopen( $jsonFile, 'a+' ) or die( 'Can\'t open file' );
    $email = $_POST['newEmail'];
    $time = time();

    $emailsArray[$time] = $email;

    $jsonEmail = json_encode( $emailsArray, JSON_PRETTY_PRINT );

    fwrite( $fh, $jsonEmail);
    fclose( $fh );
}
?>